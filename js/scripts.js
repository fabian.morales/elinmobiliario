(function(window, $){
	var seccionActual = "";
	
	var links_sig = {
		"inicio" : "link-muestra-2014",
		"muestra-2014" : "link-convocatoria-2014",
		"convocatoria-2014" : "link-info-2014",
		"info-2014" : "link-programacion-2014",
		"programacion-2014" : "link-patrocinadores",
		"patrocinadores" : "link-peliculas-invitadas",
		"peliculas-invitadas" : "link-invitados-especiales",
		"invitados-especiales" : "link-canal-macs",
		"canal-macs" : "link-prensa",
		"prensa" : "link-fundacion",
		"fundacion" : "link-archivo",
	};
	
	function scrollTo(target) {
		var wheight = $(window).height();
		
		var ooo = $(target).offset().top;
		$('html, body').animate({scrollTop:ooo}, 600);
	}
		   
	function ajusteAltoSecciones(){
		var altoHeader = $("#header-top").outerHeight(true);            
		var alto = $(window).innerHeight() - altoHeader;            
		alto = alto < 600 ? 600 : alto;
		
		$(".seccion").each(function(i, o) { $(o).css("min-height", alto + "px"); });
		//$("#inicio, #muestra-2014").css("height", alto + "px");
		$(".separador_seccion").each(function(i, o) { $(o).css("height", altoHeader + "px"); });
	}
	
	function igualarSlides(){
		var maxHeight = 0;
		$("ul#slider_info > li > div").each(function() { $(this).removeAttr('style'); });
		
		$("ul#slider_info > li > div").each(function(){
		   if ($(this).outerHeight(true) > maxHeight) { maxHeight = $(this).outerHeight(true); }
		});

		$("ul#slider_info > li > div").height(maxHeight);
	}
	
	function detectaSeccionActual(){
		$("div.seccion").each(function (i, o){
			var docViewTop = $(window).scrollTop();
			var docViewBottom = docViewTop + ($(window).height() / 2);
			var elemTop = $(o).offset().top;
			//var elemBottom = elemTop + $(o).height();
			if (elemTop >= docViewTop && elemTop <= docViewBottom){                        
				if ($(o).attr("id") !== seccionActual){
					seccionActual = $(o).attr("id");
					if (links_sig[seccionActual] != undefined){
						$("#link-sig").fadeIn();
						$("#link-sig").attr("href", "#" + links_sig[seccionActual]);    
					}
					else{
						$("#link-sig").fadeOut();
					}
				}
			}
		});
	}
	
	function hookListaInvitados(){
		if (!$("#div_bio").is(":visible")){
			$(".lista_invitados img").unbind("click");
			$(".lista_invitados img").bind("click", function(e) {
				e.preventDefault();
				var src = $(this).attr("data-src");
				var $html = $(src).html();
				if ($.trim($html) !== "" || $html != undefined){
					var tgt = $("<div></div>");
					$("div.div_bio_small").remove();
					
					tgt.addClass("div_bio_small");
					
					tgt.removeClass("div_bio_visible")
								 .fadeIn("fast", function() {
									 $(this).html($html)
											.addClass("div_bio_visible")
											.append("<a class='boton_cerrar'></a>");
									 $(this).has("a.boton_cerrar").unbind("click"); 
									 $(this).has("a.boton_cerrar").bind("click", function(e) {
										 e.preventDefault();                                             
										 tgt.fadeOut("fast", function() {
											 $(this).html("")
											 .removeClass("div_bio_visible") 
										 });
									 });
								 });
								 
					$(this).parent().append(tgt);
					scrollTo(this)
				}
			});
		}
		else{
			$(".lista_invitados img").unbind("click");
			$("div.div_bio_small").remove();
		}
	}
	
	$(document).ready(function() {
		$(window).resize(function() {                
			if ($("#top-menu-resp").is(":visible")){
				$("ul.top_menu").hide();
			}
			else{                    
				$("ul.top_menu").show();
			}
			
			hookListaInvitados();
			ajusteAltoSecciones();
			igualarSlides();
		});
		
		$(window).scroll(function () {
			detectaSeccionActual();
		});
	
		$("#top-menu-resp > a").click(function(e){
			e.preventDefault();
			$("ul.top_menu").toggle("slow");
		});
		
		$("a#logo, .top_menu a, #menu_sidebar a, a#link-sig").click(function(e) { 
			e.preventDefault();				
			scrollTo($(this).attr("href"));
		});
		
		 $("#slider_info, #slider_medios").lightSlider({
			item: 1,
			pager: false,
			enableDrag: true,
			controls: true,
			loop: true
		});
		
		var $tpl = {
			wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="my fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
			image    : '<img class="fancybox-image" src="{href}" alt="" />',
			iframe   : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0"></iframe>',
			error    : '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
			closeBtn : '<a title="Cerrar" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next     : '<a title="Siguiente" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev     : '<a title="Anterior" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		};
		
		$("a[rel='video_trailer']").fancybox({
			type: 'iframe',
			tpl : $tpl,
			padding: 0,
			arrows: false,
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
		
		$(".galeria_prensa a.ver_mas").fancybox({
			tpl : $tpl,
			padding: 0,
			arrows: true,
			type: 'iframe',
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
		
		$(".lista_invitados img").hover(function () {
			var src = $(this).attr("data-src");
			var $html = $(src).html();
			if ($.trim($html) !== "" || $html != undefined){
				$("#div_bio").removeClass("div_bio_visible")
							 .fadeIn("fast", function() {
								 $(this).html($html)
										.addClass("div_bio_visible")
										.append("<a class='boton_cerrar'></a>");
								 $(this).has("a.boton_cerrar").unbind("click"); 
								 $(this).has("a.boton_cerrar").bind("click", function(e) {
									 e.preventDefault();
									 $("#div_bio").fadeOut("fast", function() {
										 $(this).html("")
										 .removeClass("div_bio_visible") 
									 });
								 });
							 });
			}
		});
		
		$("#fundacion dd > div.content").not("#panel-aliados").niceScroll({ 
			autohidemode: false,
			cursorwidth: '10px',
			cursorcolor : "#fff",
			railpadding: { top:0, right:3, left:3, bottom:0},
			cursoropacitymax: 0.8
		});
		
		$("#panel-aliados").niceScroll({ 
			autohidemode: false,
			cursorwidth: '10px',
			cursorcolor : "#000",
			railpadding: { top:0, right:3, left:3, bottom:0},
			cursoropacitymax: 0.8
		});
		
		$(document).foundation();
		$('img[usemap]').rwdImageMaps();
		
		hookListaInvitados();
		ajusteAltoSecciones();
		igualarSlides();
		detectaSeccionActual();
	});
})(window, jQuery);